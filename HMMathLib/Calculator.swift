//
//  HMCalculator.swift
//  HMMathLib
//
//  Created by Harry Hsu on 2019/7/9.
//  Copyright © 2019 Dayu. All rights reserved.
//

import Foundation
import HMLogger

public class Calculator : NSObject {
  public func add(_ a: Int, _ b: Int) -> Int {
    Logger().log("### Logger: add")

    return a + b
  }
}
